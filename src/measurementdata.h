#pragma once

#include <ArduinoJson.h>
#include <FS.h>
#include <SPIFFS.h>

struct Configuration
{
    String ssid;
    String pass;
    String token;

    void saveConfiguration(String ssid, String pass, String token)
    {
        SPIFFS.begin();
        File file = SPIFFS.open("/conf.txt", "w");
        file.print(ssid);
        file.print('\n');
        file.print(pass);
        file.print('\n');
        file.print(token);
        file.print('\n');

        Serial.println(ssid);
        Serial.println(pass);
        Serial.println(token);

        file.close();
    }

    bool checkConfiguration()
    {
        SPIFFS.begin();
        return SPIFFS.exists("/conf.txt");
    }

    void loadConfiguration()
    {
        SPIFFS.begin();
        File file = SPIFFS.open("/conf.txt", "r");
        ssid = file.readStringUntil('\n');
        pass = file.readStringUntil('\n');
        token = file.readStringUntil('\n');

        file.close();
    }

    void deleteConfiguration()
    {
        SPIFFS.begin();
        SPIFFS.remove("/conf.txt");
        ESP.restart();
    }
} config;

class MeasurementData
{
    float tempSum = 0;
    float pressSum = 0;
    float humSum = 0;
    float pm25Sum = 0;
    float pm10Sum = 0;
    uint8_t divider = 0;

public:
    void addMeasurement(float temp, float hum, float pm25, float pm10)
    {
        tempSum += temp;
        humSum += hum;
        pm25Sum += pm25;
        pm10Sum += pm10;
        ++divider;
    }
    String getMeasurementData()
    {
        // returns serialized JSON data
        String serialized;
        DynamicJsonDocument json(64);
        json["temperature"] = tempSum / divider;
        json["humidity"] = humSum / divider;
        json["pm2_5"] = pm25Sum / divider;
        json["pm10"] = pm10Sum / divider;
        tempSum = humSum = pm25Sum = pm10Sum = divider = 0; // reset data
        serializeJson(json, serialized);

        return serialized;
    }
};