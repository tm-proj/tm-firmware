#include <Arduino.h>
/**
 * Bluetooth
 */
#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <BLE2902.h>
#include <Wire.h>
#include "bluetooth/bluetooth.h"
#include "measurementdata.h"
/**
 * SPS30
 */
#include <sps30.h>

/**
 * BME280
 */
/**
 * TODO: TODO
 */

/**
 * WiFi
 */
#include <WiFi.h>
#include <WiFiClientSecure.h>
#include <HTTPClient.h>
#include <Adafruit_BME280.h>

#define SERVICE_UUID "2aae8a3d-8c12-4a07-b05f-d101c8cb8a58"
#define UUID_RX "38643c48-7ecc-422a-acf0-ec673bfa88a7"
#define UUID_TX "b523613e-8963-4018-93ef-8ce799958cf4"

#define SP30_COMMS SERIALPORT1
#define TX_PIN 26
#define RX_PIN 27

void Errorloop(char *mess, uint8_t r);
#define MSG_BUFFER_SIZE 250
char msg[MSG_BUFFER_SIZE];
int value = 0;

extern const char cert_start[] asm("_binary_private_cert_pem_start");
extern const char device_name[] asm("_binary_private_device_name_start");

WiFiClientSecure client;

#define SERVER_PATH "https://tmback.hopto.org:443/device/save-measure"
#define SETUP_PATH "https://tmback.hopto.org:443/device/init"

SPS30 sps30;
Adafruit_BME280 bme;

BLECharacteristic *characteristicTX;
MeasurementData measurements;

#define INTEGRATION_INTERVAL 1 * 60 * 1000
#define MEASUREMENT_INTERVAL 1 * 1000
unsigned long latchedTime = 0;     // integration timer
unsigned long measurementTime = 0; // measurement timer
bool deviceConnected = false;
bool SPS_INIT = false;

void initSPS30()
{
  sps30.SetSerialPin(RX_PIN, TX_PIN);
  if (!sps30.begin(SP30_COMMS))
    Errorloop((char *)"could not initialize communication channel.", 0);

  if (!sps30.probe())
    Errorloop((char *)"could not probe / connect with SPS30.", 0);
  else
    Serial.println(F("Detected SPS30."));

  // reset SPS30 connection
  if (!sps30.reset())
    Errorloop((char *)"could not reset.", 0);

  // start measurement
  if (sps30.start())
    Serial.println(F("Measurement started"));
  else
    Errorloop((char *)"Could NOT start measurement", 0);

  SPS_INIT = true;
}

void getMeasurements()
{
  if (!SPS_INIT)
  {
    initSPS30();
  }

  int8_t ret = 0;
  struct sps_values val;

  do
  {
    ret = sps30.GetValues(&val);
    // data might not have been ready
    if (ret == ERR_DATALENGTH)
    {
      Serial.println("Data not ready");
    }
    else if (ret != ERR_OK)
    {
      Serial.println("Data not ready");
    }

  } while (ret != ERR_OK);

  measurements.addMeasurement(bme.readTemperature(), bme.readHumidity(), val.MassPM2, val.MassPM10);
}

bool WIFI_INIT = false;
std::string val = "1337";

void initWifi()
{
  config.loadConfiguration();
  WiFi.begin(config.ssid.c_str(), config.pass.c_str());
  Serial.println("Connecting");
  while (true)
  {
    delay(500);
    Serial.print(WiFi.status());
    Serial.print(".");
    if (WiFi.status() == WL_CONNECT_FAILED || WiFi.status() == WL_NO_SSID_AVAIL)
    {
      Serial.println("Connect failed");
      val = "400";
      characteristicTX->setValue(val);
      characteristicTX->notify();
      break;
    }
    else if (WiFi.status() == WL_CONNECTED)
    {
      Serial.println("Connected");
      Serial.println("Cert config started");
      Serial.print("Certificate: ");
      Serial.println(cert_start);
      client.setInsecure();
      Serial.println("Cert config finished");
      HTTPClient http;
      http.begin(client, SETUP_PATH);
      http.addHeader("device-access-token", config.token);
      http.addHeader("Content-Type", "application/json");

      int code = http.POST("{\"mac\":\"" + WiFi.macAddress() + "\"}");
      Serial.print("Got code: ");
      Serial.println(code);
      http.end();

      if (code == 201)
      {
        val = "200";
        characteristicTX->setValue(val);
        characteristicTX->notify();
      }
      else
      {
        val = "404";
        characteristicTX->setValue(val);
        characteristicTX->notify();
      }

      break;
    }
  }

  latchedTime = millis();
  WIFI_INIT = true;
}

void setup()
{
  Serial.begin(115200);
  if (!bme.begin(0x76))
  {
    Serial.println("Smutno mi bo TWI nie dziala");
  }

  Serial.print("Device: ");
  Serial.println(device_name);

  if (config.checkConfiguration())
  {
    initWifi();
  }
  else
  {
    Serial.println("BT config started");

    BLEDevice::init(device_name);
    BLEServer *server = BLEDevice::createServer();
    server->setCallbacks(new ServerCallbacks());

    BLEService *service = server->createService(SERVICE_UUID);

    characteristicTX = service->createCharacteristic(UUID_TX, BLECharacteristic::PROPERTY_NOTIFY);
    characteristicTX->addDescriptor(new BLE2902());

    BLECharacteristic *characteristic = service->createCharacteristic(UUID_RX, BLECharacteristic::PROPERTY_WRITE);
    characteristic->setCallbacks(new CharacteristicCallbacks());

    service->start();

    BLEAdvertising *advertising = server->getAdvertising();
    advertising->addServiceUUID(SERVICE_UUID);
    advertising->setScanResponse(true);
    advertising->start();

    Serial.println("BT config finished");
  }
}

void loop()
{
  if (config.checkConfiguration())
  {

    if (!WIFI_INIT)
      initWifi();

    if (WIFI_INIT && millis() - measurementTime >= MEASUREMENT_INTERVAL)
    {
      getMeasurements();
      measurementTime = millis();
    }

    if (WIFI_INIT && millis() - latchedTime >= INTEGRATION_INTERVAL)
    {
      HTTPClient http;

      http.begin(client, SERVER_PATH);
      http.addHeader("device-access-token", config.token);
      http.addHeader("Content-Type", "application/json");

      int code = http.POST(measurements.getMeasurementData().c_str());
      Serial.print("postHeap: ");
      Serial.println(esp_get_free_heap_size());
      Serial.print("HTTP status code");
      Serial.println(code);
      http.end();
      if (code == 403)
      {
        config.deleteConfiguration();
      }

      latchedTime = millis();
    }
  }
}

/**
 *  @brief : continued loop after fatal error
 *  @param mess : message to display
 *  @param r : error code
 *
 *  if r is zero, it will only display the message
 */
void Errorloop(char *mess, uint8_t r)
{
  if (r)
    Serial.println(r);
  else
    Serial.println(mess);
  ESP.restart();
}
