#pragma once

#include <Arduino.h>
#include <ArduinoJson.h>
#include "measurementdata.h"
#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <BLE2902.h>

class CharacteristicCallbacks : public BLECharacteristicCallbacks
{
    StaticJsonDocument<256> doc;
    std::string value;

    void onWrite(BLECharacteristic *characteristic)
    {
        value += characteristic->getValue();

        auto error = deserializeJson(doc, value);

        if (!error)
        {
            config.saveConfiguration(doc["ssid"].as<const char *>(), doc["pwd"].as<const char *>(), doc["token"].as<const char *>());
            value.clear();
        }
    }
};

class ServerCallbacks : public BLEServerCallbacks
{
    void onConnect(BLEServer *pServer)
    {
        Serial.println("Device connected");
    }

    void onDisconnect(BLEServer *pServer)
    {
        Serial.println("Device disconnected");
    }
};